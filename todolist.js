(function(){
    "use strict";
    var TodoApp = angular.module("TodoApp",[]);
    var TodoCtrl = function($scope){
        var vm = this;
        vm.todo = "";
        vm.list = [];
        vm.empty = 0;
        vm.addToList = function(){
            if(!vm.todo){
                vm.empty = 1;
            } else{
                vm.list.push({
                    todo: vm.todo,
                    completed: 0
                });
                console.log(vm.list);
                vm.empty = 0;
            }
        };
        //debugger;
        vm.itemDone = function($index){
            vm.list.splice($index,1);
        };
        vm.clearList = function($log){
            vm.list = [];
        }
    };
    TodoApp.controller("TodoCtrl",["$scope",TodoCtrl]);
})();